Repo for my OpenCV projects

- dist_calc_hugh program calculates the distance and angle from a camera lens to an object using shape detection technology from opencv library

- dist_test calculates the distance and angle to a shape on an image, from the camera lens, using color detection technology from openCV library 
